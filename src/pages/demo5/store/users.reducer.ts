import { AppActions } from './users.actions.ts';

interface UsersState {
  list: any[],
  error: string | null,
  pending: boolean
}
export const initialState: UsersState = {
  list: [],
  error: null,
  pending: false
};


export function usersReducer(state: UsersState, action: AppActions) {
  switch (action.type) {
    case 'loadUsers':
      return {...state, list: action.payload, pending: false, error: null}
    case 'deleteUser':
      return {
        ...state,
        list: state.list.filter(item => item.id !== action.payload),
        pending: false,
        error: null
      }
    case 'serverError':
      return {...state, list: [], pending: false, error: action.payload}
    case 'showPending':
      return {...state, pending: true}
  }
  return state;
}

