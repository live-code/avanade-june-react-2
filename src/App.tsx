import { Demo1 } from './pages/Demo1.tsx';
import { Demo2 } from './pages/Demo2.tsx';
import { Demo3 } from './pages/Demo3.tsx';
import { Demo4 } from './pages/Demo4.tsx';
import { Demo5 } from './pages/demo5/Demo5.tsx';
import { Demo6 } from './pages/Demo6.tsx';

function App() {

  return (
    <>
      <Demo6/>
    </>
  )
}

export default App
